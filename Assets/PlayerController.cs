﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    public float MaxRotationAngle;
    public float RotationSpeed;
    public float CameraLockTrigger = 0.4f;
    public GameObject HeadPrefab;
    GameObject Head;

    public GameObject Leaf;
    public float LeafSpawnChance;
    public float TimeBetweenLeafSpawn;
    float TimeSinceLastLeaf = 0.0f;

    public float GrowRate;
    public Text HeightCounter;
    public GameObject Spawner;

    public GameObject DeathMenu;
    public GameObject TitleMenu;
    public Text CurrentRoundHeightText;
    public Text RecordHeightText;

    // Vector3 Origin;
    Vector3 CameraOrigin;
    float LeftBounds, RightBounds;
    bool isCameraLocked = false;
    bool isStopped = false;

    AudioSource DeathSound;

    #if UNITY_IOS
    string controlScheme = "ios";
    #elif UNITY_ANDROID
    string controlScheme = "android";
    #else
    string controlScheme = "desktop";
    #endif

    float prevX;

    // Use this for initialization
    void Start ()
    {
        // Get refrence to audio so we can play the clip later
        DeathSound = GetComponent<AudioSource>();

        // Save original position of player and camera
        // Origin = Head.transform.position;
        CameraOrigin = Camera.main.transform.position;

        // Get right and left screen bounds
        Bounds bounds = CameraExtensions.OrthographicBounds(Camera.main);
        LeftBounds = bounds.min.x;
        RightBounds = bounds.max.x;

        Debug.Log(controlScheme);
    }

    void Update()
    {
        if (GameState.isPaused)
        {
            if (Input.GetAxis("Submit") > 0)
            {
                Restart();
            }
        }
        else if (!isCameraLocked)
        {
            TryLockCamera();
        }
    }

    // Update is called once per frame
    void FixedUpdate ()
    {
        if (!GameState.isPaused)
        {
            MovePlayer();
            GrowLeaves();
        }
    }

    void MovePlayer()
    {
        // Capture player input based on platform type
        Vector3 moveDirection = Vector3.up;
        float x = 0;
        if (controlScheme == "ios" || controlScheme == "android")
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.touches[0];
                x = touch.position.x > Screen.width/2 ? 1 : -1;
            }
        }
        else
        {
            if (Input.GetAxis("Horizontal") < -0.2)
            {
                x = -1;
            }
            else if (Input.GetAxis("Horizontal") > 0.2)
            {
                x = 1;
            }
        }

        // Calculate new move direction
        moveDirection.x = Mathf.Lerp(prevX, x, Time.deltaTime*2);
        // Store new previous x
        prevX = moveDirection.x;

        // Move player based on keys pressed / position touched
        Vector3 pos = Head.transform.position;
        pos = Vector3.Lerp(pos, pos + moveDirection, GrowRate);
        pos.x = Mathf.Clamp(pos.x, LeftBounds, RightBounds);
        Head.transform.position = pos;

        // Passively rotate player
        Vector3 angles =  Head.transform.rotation.eulerAngles;
        float newAngle = Mathf.Sin(Time.time)*MaxRotationAngle;
        Head.transform.rotation = Quaternion.Euler(0.0f, 0.0f, Mathf.Lerp(angles.z, newAngle, RotationSpeed));

        // Update height counter text
        UpdateHeightCounter(pos.y);
    }

    void GrowLeaves()
    {
        if (TimeSinceLastLeaf > TimeBetweenLeafSpawn)
        {
            Vector3 pos = Head.transform.position;
            int rand = Random.Range(1, 101);
            if (rand <= LeafSpawnChance)
            {
                Instantiate(Leaf, pos, Quaternion.identity);
            }
            TimeSinceLastLeaf = 0.0f;
        }
        else
        {
            TimeSinceLastLeaf += Time.deltaTime;
        }
    }

    void TryLockCamera()
    {
        if (Head.transform.position.y >= CameraLockTrigger)
        {
            isCameraLocked = true;
            Camera.main.SendMessage("StartFollowPlayer", Head);
            Spawner.SendMessage("StartSpawningUnits");
        }
    }

    public void Die()
    {
        // Play death sound
        DeathSound.Play();

        // Tell camera to stop tracking the player
        Camera.main.SendMessage("StopFollowPlayer");
        isCameraLocked = false;

        // Reset movement direction
        prevX = 0;

        // Tell unit spawner to stop spawning units
        // and delete the ones it currently has spawned
        Spawner.SendMessage("StopSpawningUnits");

        // Stop movement (set isDead variable?)
        GameState.isPaused = true;

        // Show Overlay w/ current height reached, and maximum height reached
        UpdateDeathMenu(Head.transform.position.y);
        DeathMenu.SetActive(true);

        // Set self to inactive
        Destroy(Head);
    }

    public void Restart()
    {
        InstantiateHead();

        // Reset player and camera positions
        Head.transform.position = Vector3.zero;
        Camera.main.transform.position = CameraOrigin;

        // Turn off DeathMenu
        DeathMenu.SetActive(false);
        TitleMenu.SetActive(false);

        // Unpause game
        GameState.isPaused = false;
    }

    void InstantiateHead()
    {
        Head = (GameObject) Instantiate(HeadPrefab, Vector3.zero, Quaternion.identity);
        Head.SendMessage("SetController", this.gameObject);

        // Set TrailRenderer sorting layer to back
        TrailRenderer tr = Head.GetComponent<TrailRenderer>();
        tr.sortingLayerName = "Trail";
    }

    void UpdateHeightCounter(float height)
    {
        height = height/10.0f; // height will be in 10cm
        HeightCounter.text = height.ToString("F2") + " m";
    }

    void UpdateDeathMenu(float height)
    {
        bool newHighestHeight = false;
        height = height / 10.0f;
        if (height > GameState.RecordHeight)
        {
            newHighestHeight = true;
            GameState.RecordHeight = height;
        }

        CurrentRoundHeightText.text = "Height: " + height.ToString("F2") + "m";
        if (newHighestHeight)
        {
            RecordHeightText.text = "New Record Height!";
        }
        else
        {
            RecordHeightText.text = "Record Height: " + GameState.RecordHeight.ToString("F2") + "m";
        }
    }
}
